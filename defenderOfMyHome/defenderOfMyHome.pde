import mqtt.*;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;

MQTTClient client;

int[][][] map = new int[1][1][3]; 

int player_width = 10;
int player_height = 10;

int last_speed_x = 0;
int last_speed_y = 0;
int speed_x = 0;
int speed_y = 0;
int time_since_last_speed_aug = 0;
int time_before_speed_aug = 100;
int max_speed = 5;

int r = 255;
int g = 0;
int b = 0;
String s = "Blovorad";

Map<String, int[]> player_array = new HashMap<String, int[]>();

void setup() 
{
  client = new MQTTClient(this);
  client.connect("mqtt://public:public@public.cloud.shiftr.io", s + "-user");
}

void draw() 
{
  background(0);
  
  stroke(0);
  for (int i = 0; i < map.length; ++i) 
  {
    for (int j = 0; j < map[i].length; ++j) 
    {
      if(map[i][j][0] != 0 && map[i][j][1] != 1 && map[i][j][2] != 2) {
        fill(map[i][j][0], map[i][j][1], map[i][j][2]);
        rect(i, j, 1, 1);
      }
    }
  }
  
  for (Map.Entry<String, int[]> entry : player_array.entrySet()) 
  {
    String pseudo = entry.getKey();
    int[] tab = entry.getValue();
    fill(tab[2], tab[3], tab[4]);
    stroke(255);
    rect(tab[0], tab[1], player_width, player_height);

    textSize(15);
    fill(255, 255, 255);
    float txt_width = textWidth(pseudo);
    float txt_height = textAscent() + textDescent();
    text(pseudo, tab[0] - txt_width / 2, tab[1] + txt_height + 5);
  }
}

void keyPressed()
{
  if(millis() - time_since_last_speed_aug < time_before_speed_aug)
    return;
    
  last_speed_x = speed_x;
  last_speed_y = speed_y;
  if (key == 'z' || key == 'Z' || keyCode == UP) 
    speed_y -= 1;
  else if (key == 'q' || key == 'Q' || keyCode == LEFT) 
    speed_x -= 1;
  else if (key == 's' || key == 'S' || keyCode == DOWN) 
    speed_y += 1;
  else if (key == 'd' || key == 'D' || keyCode == RIGHT ) 
    speed_x += 1;
  
  if(speed_x > max_speed)
    speed_x = max_speed;
  if(speed_x < -max_speed)
    speed_x = -max_speed;
  if(speed_y > max_speed)
    speed_y = max_speed;
  if(speed_y < -max_speed)
    speed_y = -max_speed;
    
  //println("x : " + x + " y : " + y);
  if(last_speed_x  != speed_x || last_speed_y != speed_y)
    client.publish("/pmusexy1/DOMHserver", speed_x + ";" + speed_y + ";" + s);
  
  time_since_last_speed_aug = millis();
}

void keyReleased()
{
  if (key == 'z' || key == 'Z' || keyCode == UP) 
    speed_y = 0;
  else if (key == 'q' || key == 'Q' || keyCode == LEFT) 
    speed_x = 0;
  else if (key == 's' || key == 'S' || keyCode == DOWN) 
    speed_y = 0;
  else if (key == 'd' || key == 'D' || keyCode == RIGHT ) 
    speed_x = 0;
  
  client.publish("/pmusexy1/DOMHserver", speed_x + ";" + speed_y + ";" + s);
}

void clientConnected() 
{
  println("client connected");
  client.subscribe("/pmusexy1/DOMHanswerserver");
  client.subscribe("/pmusexy1/DOMHanswermapsize");
  client.subscribe("/pmusexy2/addnewplayer");
  client.subscribe("/pmusexy1/launchgame");
  client.subscribe("/pmusexy1/launchgamepos" + s);
  
  client.publish("/pmusexy1/DOMHmapsize",  r + ";" + g  + ";" + b + ";" + s);
  //client.publish("/pmusexy1/DOMHrequestnewplayer", r + ";" + g  + ";" + b + ";" + s);
}

void clean_map()
{
  for (int i = 0; i < map.length; i++) 
  {
    for (int j = 0; j < map[i].length; j++) 
    {
      map[i][j][0] = 0;
      map[i][j][1] = 0;
      map[i][j][2] = 0;
    }
  }
}

void messageReceived(String topic, byte[] payload) 
{
  try
  {
    println("" + topic + " - " + new String(payload));
    if(topic.startsWith("/pmusexy1/DOMHanswerserver"))
    {
      String[] valeurs = new String(payload).split(";");
      String pseudo = new String(valeurs[2]);
      
      int[] array = player_array.get(pseudo);
      array[0] = Integer.parseInt(valeurs[0]);
      array[1] = Integer.parseInt(valeurs[1]);
      
      player_array.put(pseudo, array);
      
      map[Integer.parseInt(valeurs[0])][Integer.parseInt(valeurs[1])][0] = array[2];
      map[Integer.parseInt(valeurs[0])][Integer.parseInt(valeurs[1])][1] = array[3];
      map[Integer.parseInt(valeurs[0])][Integer.parseInt(valeurs[1])][2] = array[4];
    }
    else if(topic.startsWith("/pmusexy1/DOMHanswermapsize"))
    {
      String[] valeurs = new String(payload).split(";");
      windowResize(Integer.parseInt(valeurs[0]), Integer.parseInt(valeurs[1]));
      map = new int[width][height][3];
      
      String pseudo = new String(valeurs[5]);
      int[] array = new int[5];
      array[0] = -20;
      array[1] = -20;
      array[2] = Integer.parseInt(valeurs[2]);
      array[3] = Integer.parseInt(valeurs[3]);
      array[4] = Integer.parseInt(valeurs[4]);
      
      player_array.put(pseudo, array);
      clean_map();
    }
    else if(topic.startsWith("/pmusexy1/launchgamepos" + s))
    {
      String[] valeurs = new String(payload).split(";");
      int[] array = player_array.get(s);
      array[0] = Integer.parseInt(valeurs[0]);
      array[1] = Integer.parseInt(valeurs[1]);
      
      println("Taille map : " + map.length + " et le width = " + map[0].length);
      
      player_array.put(s, array);
    }
    else if(topic.startsWith("/pmusexy1/launchgame"))
    {
      clean_map();
    }
    else if(topic.startsWith("/pmusexy1/removeplayer"))
    {
      player_array.remove(new String(payload));
    }
    else
      println("Commande non reconnus");
  }
  catch(Exception e)
  {
    //e.printStackTrace();
  }
}

void connectionLost() 
{
  println("connection lost");
}
