import mqtt.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

MQTTClient server;

String s = "Blovorad";
List<String> player_list = new ArrayList<String>();
Map<String, int[]> player_array = new HashMap<String, int[]>();

int map_width = 800;
int map_height = 600;
int time_before_update = 100;
int[][][] map;
int tile_width = 1;
int tile_height = 1;

void setup() 
{
  //size(map_width, map_height);
  map = new int[map_width / tile_width][map_height / tile_height][3]; 
  
  server = new MQTTClient(this);
  server.connect("mqtt://public:public@public.cloud.shiftr.io", s + "-server");
}

void draw() 
{
  for (Map.Entry<String, int[]> entry : player_array.entrySet()) 
  {
    String pseudo = entry.getKey();
    int[] tab = entry.getValue();
    if(millis() - tab[4] < time_before_update)
    {
      continue;
    }
     tab[4] = millis(); 
    tab[0] += tab[2];
    tab[1] += tab[3];
    if(tab[0] > map[0].length)
      tab[0] = map[0].length - 10;
    if(tab[0] < 0)
      tab[0] = 0;
    if(tab[1] < 0)
      tab[1] = 0;
    if(tab[1] > map.length)
      tab[1] = map.length - 10;
    player_array.put(pseudo, tab);
    //server.publish("/pmusexy1/DOMHanswerserver", tab[0] + ";" + tab[1] + ";" + pseudo);
  }
}

void keyPressed()
{
  if(key == 'a')
  {
    Random r = new Random();

    for (String name : player_list) 
    {
       int y = r.nextInt(map[0].length);
       int x = r.nextInt(map.length);
       server.publish("pmusexy1/launchgamepos" + name, x + ";" + y);
    }
        server.publish("pmusexy1/launchgame", "starting new game");
  }
}

void clientConnected() 
{
  println("client connected");
  server.subscribe("/pmusexy1/DOMHserver");
  server.subscribe("/pmusexy1/DOMHmapsize");
  server.subscribe("/pmusexy1/DOMHrequestnewplayer");
}

void messageReceived(String topic, byte[] payload) 
{
    println("" + topic + " - " + new String(payload));
    
    try
    {
      if(topic.startsWith("/pmusexy1/DOMHserver"))
      {
        String[] valeurs = new String(payload).split(";");
        String pseudo = new String(valeurs[2]);
      
        int[] array = player_array.get(pseudo);
        array[2] = Integer.parseInt(valeurs[0]);
        array[3] = Integer.parseInt(valeurs[1]);
      
        player_array.put(pseudo, array);
      }
      else if(topic.startsWith("/pmusexy1/DOMHmapsize"))
      {
        server.publish("/pmusexy1/DOMHanswermapsize", map_width + ";" + map_height + ";" + new String(payload));
      }
      else if(topic.startsWith("/pmusexy1/DOMHrequestnewplayer"))
      {
        
        String[] valeurs = new String(payload).split(";");
        player_list.add(valeurs[3]);
        int[] array = new int[7];
        array[0] = 0;
        array[1] = 0;
        array[2] = 0;
        array[3] = 0;
        array[4] = millis();
        array[5] = 0;
        array[6] = 0;
        player_array.put(valeurs[3], array);
        println("We add player");
        server.publish("/pmusexy2/addnewplayer", new String(payload));
      }
    }
    catch(Exception e)
    {
    }
}

void connectionLost() 
{
  println("connection lost");
}
